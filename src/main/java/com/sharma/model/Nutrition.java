package com.sharma.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="NutritionS")
public class Nutrition implements Serializable 
{
    private static final long serialVersionUID = -889976693182180703L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private int id;
    
    @Column(name="TYPES")
    private String types;
    
    @Column(name="QUANTITY")
    private int quantity;
    
    @Column(name="MEASURE")
    private String measure;

    
    public Nutrition()
    {
        super();
    }

    public Nutrition(int id, String type, int quantity, String measure)
    {
        super();
        this.id = id;
        this.types = type;
        this.quantity = quantity;
        this.measure = measure;
    }
    
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
   
    public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	@Override
    public String toString()
    {
        return types+":"+quantity+" "+measure;
    }

	public boolean isPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	public static Nutrition get() {
		// TODO Auto-generated method stub
		return null;
	}

 
}