package com.sharma;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.sharma.model.Nutrition;
import com.sharma.service.NutritionServiceImpl;

@SpringBootApplication
@ComponentScan("com.sharma")
@EnableJpaRepositories("com.sharma.repository")
public class DemoApplication {
	
	@Autowired
	NutritionServiceImpl nutritionServiceImpl;
	
	@PostConstruct
	public void run() throws Exception{
		System.out.println("new DemoApplication().nutritionServiceImpl..."+nutritionServiceImpl);
		nutritionServiceImpl.save();
		System.out.println("now get depending on ID");
		nutritionServiceImpl.findById(5);
//		n.toString();
		nutritionServiceImpl.getAll();

	}
	
	public static void main(String[] args)
	{
	SpringApplication.run(DemoApplication.class, args);
	System.out.println("welcome to spring boot");
	
	//new DemoApplication().nutritionServiceImpl.save();

}}