package com.sharma.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sharma.model.Nutrition;

@Component
public interface NutritionService 
{
    public void save();
    public Nutrition findByIds(int id) throws Exception;
    public String deleteById(int id);
    public List<Nutrition> getAll();
}
