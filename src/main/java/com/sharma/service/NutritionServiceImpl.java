package com.sharma.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sharma.model.Nutrition;
import com.sharma.repository.NutritionRepository;

@RestController
public class NutritionServiceImpl implements NutritionService {

	@Value("${spring.application.g}")
	private String g;
	
	@Value("${spring.application.mg}")
	private String mg;
	
	@Value("${spring.application.message}")
	String message;
	
	@Value("${spring.application.pattern}")
	String pattern;

	@Autowired
	
	NutritionRepository nutritionRepository;
	
	
	@Override
	public void save()
	{

	        String nutrionDetails="Calories 65 ;fat 31 g (saturtated 6 g);Cholestrol 103 mg ;Sodium 974 mg;Carbohydrate 54 g;fiber 4 g;Sugar 13 g;Protien 40 g";
	        
	        Pattern regExPattern = Pattern.compile(pattern);

	        List<String> nutritionList = new ArrayList<String>();
	        List<Nutrition> lstNut =  new ArrayList<Nutrition>();

	        nutritionList = regExPattern.splitAsStream(nutrionDetails ).filter(e -> !e.isEmpty()).collect(Collectors.toList());
	        try
	        {
	        nutritionList.forEach( net->{
	        	Nutrition nut =new Nutrition();
	            System.out.print(net+"\n");
	            Pattern nutritionSplitter = Pattern.compile("\\s");
	            List<String> nutritionContent=  nutritionSplitter.splitAsStream(net).collect(Collectors.toList());
	            System.out.print(nutritionContent+"\n");
	                     	
	            	 nut.setTypes(nutritionContent.get(0));
	            	 nut.setQuantity(Integer.parseInt(nutritionContent.get(1)));
	            	 if(nutritionContent.size()==3)
	            		 {
	            		 if(nutritionContent.get(2).equals("g"))
	            			 nutritionContent.set(2,g);
	            		 else if(nutritionContent.get(2).equals("mg"))
	            			 nutritionContent.set(2,mg);
	            		 nut.setMeasure(nutritionContent.get(2));
	            		 }
	            	 else
	            		 nut.setMeasure("");
	            	 lstNut.add(nut);
	 	            System.out.print(nutritionContent+"\n");

	        
	        });
	        nutritionRepository.saveAll(lstNut);
	        }
	        catch(Exception e) {
				System.out.println(message+e.getMessage());
	        }
	}
	
    @RequestMapping(value="/nutrition/{id}", method = RequestMethod.GET)
	public Nutrition findById(@PathVariable int id) throws Exception {
		Optional<Nutrition> nutOptional = nutritionRepository.findById(id);
		Nutrition nut =new Nutrition();
    	if(nutOptional.isPresent()){
        	 nut =nutOptional.get();
    	}
    	return nut;
	}
	
    @DeleteMapping("/students/{id}")
    public String deleteById(@PathVariable int id) {
    	nutritionRepository.deleteById(id);
    	return "deleted";
    }
	
	@Override
    @RequestMapping(value="/nutrition", method = RequestMethod.GET)
    public List<Nutrition> getAll()
    {
    	return nutritionRepository.findAll();
    }

	@Override
	public Nutrition findByIds(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	


}
