package com.sharma.service;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.sharma.model.Nutrition;

public class NutritionServiceImplTest {
	
	@Mock
	NutritionServiceImpl nutSer;

	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);

	}
	

   @Test
	public void getByIdTest() throws Exception {
	 Nutrition nut =new Nutrition();
	
	 nut.setId(5);
	 nut.setTypes("Sodium");
	 nut.setQuantity(974);
	 nut.setMeasure("Milligrams");

     when(nutSer.findById(5)).thenReturn(nut);

   }
   public void deleteById()
   {
//   when(nutSer.deleteById(5)).thenReturn("deleted");
   
//   Assert.failNotSame("nut",
//           () -> assertEquals("Jane", nut.getTypes()),
//           () -> assertEquals("Doe", nut.getMeasure())
//       );
   Assert.assertEquals("deleted", nutSer.deleteById(5));
   Mockito.verify(nutSer).deleteById(5);
   System.out.println("😱");
   }
   
	@Test
	public void test() {
			System.out.println("This test Ran");
			Assert.assertTrue(true);
			
	}

}
